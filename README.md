# Join deliberate - Frontend Challenge

Thanks for your interest in joining Deliberate! Please, check our open positions at https://www.deliberate.ai/careers

## Goals

This coding challenge was designed to allow you to showcase your React skills! The purpose of this exercise is to demonstrate your ability to build a greenfield project with what you consider best practices.

## Overview

Clear communication between healthcare professionals and patients is critical to improving health and the quality of the healthcare system. Health literacy is a US government project aiming to improve the communication between health professionals and patients. They create, promote, and curate evidence-based health literacy and communication tools, practices, and research for health professionals. For this project, you'll use provided resources to find effective strategies for sharing health information in ways that people can understand and use.

## Assignment

Create a React application that allows health professionals to search for prevention and wellness resources.

### Feature

- Consume the MyHealthfinder API `/topicsearch` for accessing Health Topics. Documentation can be found [here](https://health.gov/our-work/national-health-initiatives/health-literacy/consumer-health-content/free-web-content/apis-developers/api-documentation)
- Create an autocomplete filter by _keyword_
- You are free to create the layout

Note: We will **NOT** evaluate your design skills

### Data

Display the following fields for every result:

- Title
- Categories
- Image
  - ImageUrl
  - ImageAlt
- RelatedItems
  - Title
  - Url
- Sections
  - Title
  - Content

## Guidelines

The challenge is intentionally kept very open. Feel free to make your own decisions and be creative. If you don't get to work on everything, outline what the next steps would look like and how you would approach them.

**Important**: You can use any React library you wish. The only requirement is that you use Typescript.

Questions to consider:

- Does it work as expected?
- Are there automated tests?
- Does the code follow well know best practices?
- Is it well documented? What is the building and deploying process?
- Is the application using resources properly? What happens if the user types too fast?
- Are there any security vulnerabilities?
- Is it production-ready?
- Did I document my decisions?
- If something is missing, does the README explain why it is missing?

# Submission

Once you are comfortable with your code, please provide the GitLab link. If it is a private one, please invite felipe@deliberate.ai and yuri@deliberate.ai.

This is a limited-time challenge. Please, submit it within the time window discussed via email.

# Extra Challenge

Filter all topics by _keyword_ and _category_.

- List all categories consuming the API MyHealthfinder `/itemlist`
- Display the _title_ for every result
- Allow the users to select a category in the list and filter the topic search
- The user should still be able to filter without using a category
